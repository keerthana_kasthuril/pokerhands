import unittest
from card import Card



class TestCards(unittest.TestCase):
    def test_card(self):
        card = Card('2H')
        self.assertEqual(card.value, 2)
        self.assertEqual(card.suit, "Hearts")
        self.assertEqual(card.valueSymbol, "2")

    def test_card_10(self):
        card = Card('10C')
        self.assertEqual(card.value, 10)
        self.assertEqual(card.suit, "Clubs")
        self.assertEqual(card.valueSymbol, "10")

    def test_symbol_card(self):
        card = Card('AS')
        self.assertEqual(card.value, 14)
        self.assertEqual(card.suit, "Spades")
        self.assertEqual(card.valueSymbol, "A")
