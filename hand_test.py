import unittest
from hand import Hand
from card import Card


class TestHand(unittest.TestCase):
    def test_hand(self):
        hand = Hand("2S 5H 6D AC 4S")
        self.assertTrue(all(isinstance(card, Card) for card in hand.cards))
