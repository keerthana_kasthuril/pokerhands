class Card:
    valueDictionary = {"J": 11, "Q": 12, "K": 13, "A": 14}
    suitDictionary = {"H": "Hearts", "D": "Diamonds", "C": "Clubs", "S": "Spades"}

    def __init__(self, card):
        self.suit = self.suitDictionary.get(card[-1])
        self.value = int(self.valueDictionary.get(card[:-1], card[:-1]))
        self.valueSymbol = card[:-1]
