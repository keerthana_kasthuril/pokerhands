import unittest
from game import Game


class TestGame(unittest.TestCase):
    def test_simple_high_card_case(self):
        gameHands = Game("2H 7C AS QD 5C", "KD 3D 6C 9H 10D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "2 Hearts,5 Clubs,7 Clubs,Q Diamonds,A Spades,")

    def test_high_card_when_both_has_same_highest_card(self):
        gameHands = Game("2H 7C KS JD 5C", "KD 3D 6C QH 10D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "3 Diamonds,6 Clubs,10 Diamonds,Q Hearts,K Diamonds,")

    def test_high_card_when_only_least_value_differs(self):
        gameHands = Game("2H 7C KS JD 5C", "KD 3D JC 7H 5D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "3 Diamonds,5 Diamonds,7 Hearts,J Clubs,K Diamonds,")

    def test_high_card_when_both_hands_are_same(self):
        gameHands = Game("2H 7C KS JD 5C", "KD 7D 5H JH 2D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "It's a tie")

    def test_one_pair_case(self):
        gameHands = Game("2H 7C KS JD 5C", "QD 3D 6C QH 10D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "3 Diamonds,6 Clubs,10 Diamonds,Q Diamonds,Q Hearts,")

    def test_one_pair_when_both_has_a_pair(self):
        gameHands = Game("3H 7C KS JD 3C", "QD 3D 6C QH 10D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "3 Diamonds,6 Clubs,10 Diamonds,Q Diamonds,Q Hearts,")

    def test_one_pair_when_both_has_equal_pair(self):
        gameHands = Game("3H 7C KS JD 3C", "3S 3D 6C QH 10D")

        winner = gameHands.get_winner()

        self.assertEqual(winner, "3 Hearts,3 Clubs,7 Clubs,J Diamonds,K Spades,")
