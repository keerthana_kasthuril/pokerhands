from hand import Hand


class Game:
    def __init__(self, hand1, hand2):
        self.hand1 = Hand(hand1)
        self.hand2 = Hand(hand2)

    def get_winner(self):
        rankForHand1 = self.hand1.check_one_pair()
        rankForHand2 = self.hand2.check_one_pair()
        if rankForHand1 == rankForHand2:
            return self.hand1.resolve_tie(self.hand2)
        return self.hand1.get_hand_details() \
            if rankForHand1 > rankForHand2 \
            else self.hand2.get_hand_details()

    def check_pairs(self):
        pairCheckForHand1 = self.hand1.check_one_pair()
        pairCheckForHand2 = self.hand2.check_one_pair()
        if pairCheckForHand1 == 1 and pairCheckForHand2 == 0:
            return self.hand1
        elif pairCheckForHand1 == 0 and pairCheckForHand2 == 1:
            return self.hand2
        return -1
