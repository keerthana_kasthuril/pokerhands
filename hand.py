from card import Card
from collections import Counter


class Hand:
    def __init__(self, inputHand):
        self.rank = 0
        self.cards = []
        hand = inputHand.split(" ")
        for i in range(len(hand)):
            self.cards.append(Card(hand[i]))
        self.cards.sort(key=lambda cards: cards.value)

    def get_value_list(self):
        values = []
        for i in range(len(self.cards)):
            values.append(self.cards[i].value)
        return values

    def get_hand_details(self):
        hand_details = ""
        for i in range(len(self.cards)):
            hand_details += self.cards[i].valueSymbol + " " + self.cards[i].suit + ","
        return hand_details

    def get_high_card(self, opponentHand):
        for i in range(len(self.cards) - 1, -1, -1):
            if self.cards[i].value == opponentHand.cards[i].value:
                continue
            return self.get_hand_details() \
                if self.cards[i].value > opponentHand.cards[i].value \
                else opponentHand.get_hand_details()
        return "It's a tie"

    def check_one_pair(self):
        uniqueCardSet = set(self.get_value_list())
        if len(uniqueCardSet) == 4:
            self.rank = 1
            return 1
        return 0

    def resolve_tie(self, opponentHand):
        if self.rank == 1:
            pairValue = self.get_pair_value()
            opponentPairValue = opponentHand.get_pair_value()
            if pairValue > opponentPairValue:
                return self.get_hand_details()
            elif pairValue < opponentPairValue:
                return opponentHand.get_hand_details()
        return self.get_high_card(opponentHand)

    def get_pair_value(self):
        frequency = Counter(self.get_value_list())
        for i in frequency:
            if frequency[i] == 2:
                return i
